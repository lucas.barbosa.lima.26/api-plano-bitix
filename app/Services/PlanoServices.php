<?php 

namespace App\services;

class PlanoServices {
    public function verificarCliente ($beneficiarios){
        
        $arquivo = file_get_contents(resource_path('json/planos.json'));//file_get_contents usado para ler arquivo planos.jon
        $arquivo2 = file_get_contents(resource_path('json/precos.json'));
        $planos = json_decode($arquivo);//json_decode convertendo variavel arquivo com conteudo json para PHP
        $precos = json_decode($arquivo2);
        $count = count($beneficiarios);//count esta retornando usuarios existem dentro de um array



        foreach ($beneficiarios as $key=>$beneficiario){// iniciando loop de repetição para cara beneficiario
            
            $response  = $this -> encontrarPlano($planos,$beneficiarios, $beneficiario, $key);
            $beneficiario ["codigo"] = $response[0]; // $response[1] retorna codigo //codigo de beneficiario indicado para pesquisa no arquivo preco.jon
            $responsePreco = $this -> encontrarPreco($beneficiario,$precos, $count);//afunção encontrarPreco evia tres valores para encontrar preco e armazenar em responcePreco
            $beneficiarios[ $key ] ["plano"] = $response[1];// $response[1] retorna plano //indicando a posição de plano dentro da array de benficiarios
            $beneficiarios[ $key ] ["preco"]  = $responsePreco;//indicando a posicção de preco detro da array de beneficiarios
           
        }

        $calculo = $this -> calculaPlano($beneficiarios);//variavel recebendo dados da função calculaPano

        return ["Beneficiarios" => $beneficiarios, "Total" => $calculo]; //retorno da array de cada variavel
         
    }

    public function encontrarPlano($planos, $beneficiarios, $beneficiario, $key){//encotrar planos

        foreach ($planos as &$plano){//looop de plano
               
            if($beneficiario['registro'] == $plano->registro){
           
                  
               $beneficiarios[ $key ] ["plano"] = $plano -> nome;
               return [$plano->codigo,$plano -> nome] ;
               
            }

        }

    }

    public function encontrarPreco($beneficiario, $precos, $count){//encontrar preco
        $valor = 0;
        foreach ($precos as &$preco){//loop de preco
               
            if( $beneficiario ["codigo"]  == $preco->codigo && $count >= $preco -> minimo_vidas){   
 
                if($beneficiario["idade"] >0 && $beneficiario["idade"] <= 17 ){
                     $valor = floatval($preco -> faixa1);
                       
                }elseif($beneficiario["idade"] >= 18  && $beneficiario["idade"] <= 40){
                    $valor = floatval($preco -> faixa2);
                    

                }elseif ($beneficiario["idade"] > 40) {
                    $valor = floatval($preco -> faixa3);
                   
                }
            }
        }
        return $valor;
    }
    public function calculaPlano($beneficiarios){//calcular plano
        $total = 0;
        foreach($beneficiarios as &$beneficiario){//loop de beneficiario
            $total = $beneficiario["preco"] + $total;
        }
        return $total;

    }

    

}


