<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
 use App\Services\PlanoServices;

class PlanoController extends Controller
{
    private $plano;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PlanoServices $plano)//__construct instanciando PlanoService
     {
         $this->plano = $plano;
     }

    public function store(Request $request){//store recebe json e retorna em json
         $request -> beneficiarios ;
        try {
            $response =  $this -> plano -> verificarCliente($request -> beneficiarios);
            return response()->json($response);
            
        } catch (\PDOException $e) {
            return response()->json(['msgerro'=>$e->getMessage()],406);
        }
    }
    
}
