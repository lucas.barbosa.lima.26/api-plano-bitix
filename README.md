#  Bitilix
    Informando array de beneficiarios em json, conforme formato de exemplo logo a baixo, retornando valor ,plano escolhido, valor para cada beneficiario e total de todos os beneficiarios, depedendo da idade de cada beneficiario

## Formato de Json de entrada
{
	"beneficiarios":[
		{
			"nome":"Lucas",
			"idade":27,
			"registro":"reg6"
		},
		{
			"nome":"miguel",
			"idade":1,
			"registro":"reg1"	
		},
		{
			"nome":"Angelica",
			"idade":29,
			"registro":"reg1"
		}
	]
}
# Backend

Para ativar o servidor de teste
```
php -S localhost:8000 -t public
```
A API será levantada em http://localhost:8000/PlanoCotacao

## Verções das Dependências dos Projetos
PHP 7.4
Composer 2.1.5

# Lumen PHP Framework

[![Build Status](https://travis-ci.org/laravel/lumen-framework.svg)](https://travis-ci.org/laravel/lumen-framework)
[![Total Downloads](https://img.shields.io/packagist/dt/laravel/framework)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Stable Version](https://img.shields.io/packagist/v/laravel/framework)](https://packagist.org/packages/laravel/lumen-framework)
[![License](https://img.shields.io/packagist/l/laravel/framework)](https://packagist.org/packages/laravel/lumen-framework)

Laravel Lumen is a stunningly fast PHP micro-framework for building web applications with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Lumen attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as routing, database abstraction, queueing, and caching.

## Official Documentation

Documentation for the framework can be found on the [Lumen website](https://lumen.laravel.com/docs).

## Contributing

Thank you for considering contributing to Lumen! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Lumen, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Lumen framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
